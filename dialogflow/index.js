

'use strict';

const functions = require('firebase-functions');
const { WebhookClient } = require('dialogflow-fulfillment');
const { Card, Suggestion } = require('dialogflow-fulfillment');
const {Image, dialogflow, Carousel, MediaObject, SimpleResponse,Suggestions, LinkOutSuggestion} = require('actions-on-google');
var apiClient  = require('request');
var audioconcat = require('audioconcat');

process.env.DEBUG = 'dialogflow:debug'; // enables lib debugging statements

const imageUrl = 'https://developers.google.com/actions/images/badges/XPM_BADGING_GoogleAssistant_VER.png';
const linkUrl = 'https://assistant.google.com/';


exports.dialogflowFirebaseFulfillment = functions.https.onRequest((request, response) => {
    const agent = new WebhookClient({ request, response });
//   console.log('Dialogflow Request headers: ' + JSON.stringify(request.headers));
//   console.log('Dialogflow Request body: ' + JSON.stringify(request.body));

    function googleAssistantOther(agent) {
        let conv = agent.conv();
        const hasMediaPlayback = conv.surface.capabilities.has('actions.capability.MEDIA_RESPONSE_AUDIO');
        const hasVideoPlayback = conv.surface.capabilities.has('actions.capability.SCREEN_OUTPUT');
        console.log("video capability is : ");
        console.log(hasVideoPlayback);
        var agentResponse = 'Do you want ';
        var receivedData = request.body.queryResult.parameters;
        if (typeof receivedData == 'undefined' || receivedData ===""){
            conv.ask('Sorry there is mp3 to play.');
            agent.add(conv);
            return;
        }
        var lovappUrl = "https://lovappy.com/api/v1/radio?page=1&limit=18";
        var gender = 'Male and Female';
        if (receivedData.Gender.length > 0){
            gender = receivedData.Gender[0];
            if (typeof gender !== 'undefined' && gender !== ""){
                gender = gender.trim();
                agentResponse = agentResponse + gender + ', ';
                if (gender == 'female'){
                    lovappUrl = lovappUrl + "&gender=FEMALE";
                } else {
                    lovappUrl = lovappUrl + "&gender=MALE" ;
                }
            }
        }


        var height = receivedData.Height;
        if (typeof height !== 'undefined' && height !== ""){
            height = height.trim();
            agentResponse = agentResponse + height + ' height, ';
            lovappUrl = lovappUrl + "&height=" +  height;
        }

        var breast = receivedData.Breast_size;
        if (typeof breast !== 'undefined' && breast !== ""){
            breast = breast.trim();
            agentResponse = agentResponse + breast + ' breast size, ';
            lovappUrl = lovappUrl + "&breastsSize=" +  breast;
        }

        var butt = receivedData.Butt_size;
        if (typeof butt !== 'undefined' && butt !== ""){
            butt = butt.trim();
            agentResponse = agentResponse + butt + ' butt size dating partner';
            lovappUrl = lovappUrl + "&buttsSize=" +  butt;
        }

        var minAge = receivedData.min_age;
        if (typeof minAge !== 'undefined' && minAge !== ""){
            minAge = minAge;
            lovappUrl = lovappUrl + "&ageFrom=" + minAge;
        }

        var maxAge = receivedData.max_age;
        if (typeof maxAge !== 'undefined' && maxAge !== ""){
            maxAge = maxAge;
            lovappUrl = lovappUrl + "&ageTo=" + maxAge;
        }

        var location = receivedData["geo-country"];
        if (typeof location !== 'undefined' && location !== ""){
            location = location.trim();
            agentResponse = agentResponse + ' from ' + location + ' as your dating partner:';
        }

        console.log("lovappy url is : ");
        console.log(lovappUrl);

        var rp = require('request-promise-native');
        var options = {
            method: 'get',
            uri: lovappUrl,
            json: true
        };


        return rp( options )
            .then( body => {

                var profiles = body.radioPage.content;
                // if (!conv.surface.capabilities.has('actions.capability.SCREEN_OUTPUT')) {
                //   conv.ask('Sorry, try this on a screen device or select the ' +
                //     'phone surface in the simulator.');
                //     agent.add(conv);
                //   return;
                // }
                conv.ask(new Suggestions('Suggestion Chips'));
                conv.ask("User audio is playing");
                var nextFileList = [];

                for(var audioCount = 1; audioCount < profiles.length;audioCount++){
                    nextFileList.push(profiles[audioCount].fileUrl + ';'+ profiles[audioCount].coverImageUrl);
                }

                agent.context.set({
                    'name': 'filesToPlay',
                    'lifespan': 14,
                    'parameters': {
                        'parameter':nextFileList
                    }
                });

                if (profiles.length > 0 && hasVideoPlayback){
                    conv.ask(new MediaObject({
                        url: profiles[0].fileUrl,
                        icon: new Image({
                            url: profiles[0].coverImageUrl,
                            alt: 'Profile view',
                        }),
                    }));
                } else if (profiles.length > 0) {
                    conv.ask(new MediaObject({
                        url: profiles[0].fileUrl,
                    }));
                } else {
                    conv.ask("No audio is found");
                }
                agent.add(conv);
                return Promise.resolve( true );
            })
            .catch( err => {
                console.log(err);
                conv.ask("Error occured");
                agent.add(conv);
                return Promise.resolve( true );
            });

    }

    function welcome(agent) {
        agent.add(`Welcome to dating agent!`);
    }

    function fallback(agent) {

        let conv = agent.conv();
        const hasVideoPlayback = conv.surface.capabilities.has('actions.capability.SCREEN_OUTPUT');
        var previousContext;
        if (hasVideoPlayback) {
            previousContext = request.body.queryResult.outputContexts[1].parameters.parameter;
        } else {
            previousContext = request.body.queryResult.outputContexts[0].parameters.parameter;
        }

        var fileCount = previousContext.length;
        var fileUrl;
        console.log("file count is : ");
        console.log(fileCount);
        if (fileCount > 0){
            fileUrl = previousContext[0];
            var fileArray = fileUrl.split(";");
            conv.ask(new Suggestions('Suggestion Chips'));
            conv.ask("Next user audio is playing");
            if (hasVideoPlayback){
                conv.ask(new MediaObject({
                    url: fileArray[0],
                    icon: new Image({
                        url: fileArray[1],
                        alt: 'Profile view',
                    }),
                }));
            } else {
                conv.ask(new MediaObject({
                    url: fileArray[0],
                }));
            }

        } else {
            conv.ask("All suggestions were played.");
        }

        if (fileCount > 1){
            previousContext = previousContext.slice(1, fileCount);
        } else {
            previousContext = [];
        }

        agent.context.set({
            'name': 'filesToPlay',
            'lifespan': 14,
            'parameters': {
                'parameter':previousContext
            }
        });

        agent.add(conv);
        return;
    }

    function other(agent) {
        agent.add(`This message is from Dialogflow's Cloud Functions for Firebase editor!`);
        agent.add(new Card({
                title: `Title: this is a card title`,
                imageUrl: imageUrl,
                text: `This is the body text of a card.  You can even use line\n  breaks and emoji! 💁`,
                buttonText: 'This is a button',
                buttonUrl: linkUrl
            })
        );
        agent.add(new Suggestion(`Quick Reply`));
        agent.add(new Suggestion(`Suggestion`));
        agent.setContext({ name: 'weather', lifespan: 2, parameters: { city: 'Rome' }});
    }

    // Run the proper handler based on the matched Dialogflow intent
    let intentMap = new Map();
    intentMap.set('Default Welcome Intent', welcome);
    intentMap.set('Default Fallback Intent', fallback);
    if (agent.requestSource === agent.ACTIONS_ON_GOOGLE) {
        intentMap.set(null, googleAssistantOther);
    } else {
        intentMap.set(null, other);
    }
    agent.handleRequest(intentMap);
});
/**
 * Copyright 2017 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

'use strict';

const functions = require('firebase-functions');
const { WebhookClient } = require('dialogflow-fulfillment');
const { Card, Suggestion } = require('dialogflow-fulfillment');
const {Image, dialogflow, Carousel, MediaObject, SimpleResponse,Suggestions, LinkOutSuggestion} = require('actions-on-google');
var apiClient  = require('request');
var audioconcat = require('audioconcat');

process.env.DEBUG = 'dialogflow:debug'; // enables lib debugging statements

const imageUrl = 'https://developers.google.com/actions/images/badges/XPM_BADGING_GoogleAssistant_VER.png';
const linkUrl = 'https://assistant.google.com/';


exports.dialogflowFirebaseFulfillment = functions.https.onRequest((request, response) => {
    const agent = new WebhookClient({ request, response });
//   console.log('Dialogflow Request headers: ' + JSON.stringify(request.headers));
//   console.log('Dialogflow Request body: ' + JSON.stringify(request.body));

    function googleAssistantOther(agent) {
        let conv = agent.conv();
        const hasMediaPlayback = conv.surface.capabilities.has('actions.capability.MEDIA_RESPONSE_AUDIO');
        const hasVideoPlayback = conv.surface.capabilities.has('actions.capability.SCREEN_OUTPUT');
        console.log("video capability is : ");
        console.log(hasVideoPlayback);
        var agentResponse = 'Do you want ';
        var receivedData = request.body.queryResult.parameters;
        if (typeof receivedData == 'undefined' || receivedData ===""){
            conv.ask('Sorry there is mp3 to play.');
            agent.add(conv);
            return;
        }
        var lovappUrl = "https://lovappy.com/api/v1/radio?page=1&limit=18";
        var gender = 'Male and Female';
        if (receivedData.Gender.length > 0){
            gender = receivedData.Gender[0];
            if (typeof gender !== 'undefined' && gender !== ""){
                gender = gender.trim();
                agentResponse = agentResponse + gender + ', ';
                if (gender == 'female'){
                    lovappUrl = lovappUrl + "&gender=FEMALE";
                } else {
                    lovappUrl = lovappUrl + "&gender=MALE" ;
                }
            }
        }


        var height = receivedData.Height;
        if (typeof height !== 'undefined' && height !== ""){
            height = height.trim();
            agentResponse = agentResponse + height + ' height, ';
            lovappUrl = lovappUrl + "&height=" +  height;
        }

        var breast = receivedData.Breast_size;
        if (typeof breast !== 'undefined' && breast !== ""){
            breast = breast.trim();
            agentResponse = agentResponse + breast + ' breast size, ';
            lovappUrl = lovappUrl + "&breastsSize=" +  breast;
        }

        var butt = receivedData.Butt_size;
        if (typeof butt !== 'undefined' && butt !== ""){
            butt = butt.trim();
            agentResponse = agentResponse + butt + ' butt size dating partner';
            lovappUrl = lovappUrl + "&buttsSize=" +  butt;
        }

        var minAge = receivedData.min_age;
        if (typeof minAge !== 'undefined' && minAge !== ""){
            minAge = minAge;
            lovappUrl = lovappUrl + "&ageFrom=" + minAge;
        }

        var maxAge = receivedData.max_age;
        if (typeof maxAge !== 'undefined' && maxAge !== ""){
            maxAge = maxAge;
            lovappUrl = lovappUrl + "&ageTo=" + maxAge;
        }

        var location = receivedData["geo-country"];
        if (typeof location !== 'undefined' && location !== ""){
            location = location.trim();
            agentResponse = agentResponse + ' from ' + location + ' as your dating partner:';
        }

        console.log("lovappy url is : ");
        console.log(lovappUrl);

        var rp = require('request-promise-native');
        var options = {
            method: 'get',
            uri: lovappUrl,
            json: true
        };


        return rp( options )
            .then( body => {

                var profiles = body.radioPage.content;
                // if (!conv.surface.capabilities.has('actions.capability.SCREEN_OUTPUT')) {
                //   conv.ask('Sorry, try this on a screen device or select the ' +
                //     'phone surface in the simulator.');
                //     agent.add(conv);
                //   return;
                // }
                conv.ask(new Suggestions('Suggestion Chips'));
                conv.ask("User audio is playing");
                var nextFileList = [];

                for(var audioCount = 1; audioCount < profiles.length;audioCount++){
                    nextFileList.push(profiles[audioCount].fileUrl + ';'+ profiles[audioCount].coverImageUrl);
                }

                agent.context.set({
                    'name': 'filesToPlay',
                    'lifespan': 14,
                    'parameters': {
                        'parameter':nextFileList
                    }
                });

                if (profiles.length > 0 && hasVideoPlayback){
                    conv.ask(new MediaObject({
                        url: profiles[0].fileUrl,
                        icon: new Image({
                            url: profiles[0].coverImageUrl,
                            alt: 'Profile view',
                        }),
                    }));
                } else if (profiles.length > 0) {
                    conv.ask(new MediaObject({
                        url: profiles[0].fileUrl,
                    }));
                } else {
                    conv.ask("No audio is found");
                }
                agent.add(conv);
                return Promise.resolve( true );
            })
            .catch( err => {
                console.log(err);
                conv.ask("Error occured");
                agent.add(conv);
                return Promise.resolve( true );
            });

    }

    function welcome(agent) {
        agent.add(`Welcome to dating agent!`);
    }

    function fallback(agent) {

        let conv = agent.conv();
        const hasVideoPlayback = conv.surface.capabilities.has('actions.capability.SCREEN_OUTPUT');
        var previousContext;
        if (hasVideoPlayback) {
            previousContext = request.body.queryResult.outputContexts[1].parameters.parameter;
        } else {
            previousContext = request.body.queryResult.outputContexts[0].parameters.parameter;
        }

        var fileCount = previousContext.length;
        var fileUrl;
        console.log("file count is : ");
        console.log(fileCount);
        if (fileCount > 0){
            fileUrl = previousContext[0];
            var fileArray = fileUrl.split(";");
            conv.ask(new Suggestions('Suggestion Chips'));
            conv.ask("Next user audio is playing");
            if (hasVideoPlayback){
                conv.ask(new MediaObject({
                    url: fileArray[0],
                    icon: new Image({
                        url: fileArray[1],
                        alt: 'Profile view',
                    }),
                }));
            } else {
                conv.ask(new MediaObject({
                    url: fileArray[0],
                }));
            }

        } else {
            conv.ask("All suggestions were played.");
        }

        if (fileCount > 1){
            previousContext = previousContext.slice(1, fileCount);
        } else {
            previousContext = [];
        }

        agent.context.set({
            'name': 'filesToPlay',
            'lifespan': 14,
            'parameters': {
                'parameter':previousContext
            }
        });

        agent.add(conv);
        return;
    }

    function other(agent) {
        agent.add(`This message is from Dialogflow's Cloud Functions for Firebase editor!`);
        agent.add(new Card({
                title: `Title: this is a card title`,
                imageUrl: imageUrl,
                text: `This is the body text of a card.  You can even use line\n  breaks and emoji! 💁`,
                buttonText: 'This is a button',
                buttonUrl: linkUrl
            })
        );
        agent.add(new Suggestion(`Quick Reply`));
        agent.add(new Suggestion(`Suggestion`));
        agent.setContext({ name: 'weather', lifespan: 2, parameters: { city: 'Rome' }});
    }

    // Run the proper handler based on the matched Dialogflow intent
    let intentMap = new Map();
    intentMap.set('Default Welcome Intent', welcome);
    intentMap.set('Default Fallback Intent', fallback);
    if (agent.requestSource === agent.ACTIONS_ON_GOOGLE) {
        intentMap.set(null, googleAssistantOther);
    } else {
        intentMap.set(null, other);
    }
    agent.handleRequest(intentMap);
});
