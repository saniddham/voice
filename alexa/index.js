/* eslint-disable  func-names */
/* eslint quote-props: ["error", "consistent"]*/
/**
 * This sample demonstrates a simple skill built with the Amazon Alexa Skills
 * nodejs skill development kit.
 * This sample supports multiple lauguages. (en-US, en-GB, de-DE).
 * The Intent Schema, Custom Slots and Sample Utterances for this skill, as well
 * as testing instructions are located at https://github.com/alexa/skill-sample-nodejs-fact
 **/

'use strict';
const Alexa = require('alexa-sdk');
const https = require('https');
const urlExists = require('url-exists');
// const AmazonSpeech = require('ssml-builder/amazon_speech');

const handlers = {
    'LaunchRequest': function () {
        this.emit(':ask', 'Welcome to love happy, please specify the partner you want to find?');
    },
    'GetDatingVoice': function () {

        var explainType = this.event.request.intent.slots.explain.value;
        console.log('this is the explain type' + explainType);
        if (typeof explainType !== 'undefined' && explainType !== ""){
            var explain = this.event.request.intent.slots.explain.resolutions.resolutionsPerAuthority[0].values[0].value.name;
            explain = explain.trim();
            if (explain == 'explain'){
                this.emit(':ask', 'Please specify the prefered specifications like gender, location, language, butt size , breast size etc', 'You can say something like find me women in england');
                //  this.emit(':ask', 'How can I help ?', 'You can say something like...');
            } else if(explain == 'join') {
                this.emit(':ask', 'Please register to lovappy and search for the dating partners');
            }   
        }
        
        var lovappUrl = "/api/v1/radio?limit=18";
        var alexaBaseUrl = "https://storage.googleapis.com/lovappy-lovdrop-alexa-testing/";
        var genderValue = this.event.request.intent.slots.gender.value;
        console.log('this is the gender of the request' + genderValue);
        if (typeof genderValue !== 'undefined' && genderValue !== ""){
            var gender = this.event.request.intent.slots.gender.resolutions.resolutionsPerAuthority[0].values[0].value.name;
            gender = gender.trim();
            if (gender == 'female'){
                lovappUrl = lovappUrl + "&gender=FEMALE"; 
            } else if(gender == 'male') {
                lovappUrl = lovappUrl + "&gender=MALE" ;
            }   
        }
        
        var breastValue = this.event.request.intent.slots.Breast_size.value;
        console.log('this is the breast size of the request' + breastValue);
        if (typeof breastValue !== 'undefined' && breastValue !== ""){
            var breastSize = this.event.request.intent.slots.Breast_size.resolutions.resolutionsPerAuthority[0].values[0].value.name;
            breastSize = breastSize.trim();
            lovappUrl = lovappUrl + "&breastsSize=" +  breastSize;  
        }
        
        var buttValue = this.event.request.intent.slots.Butt_size.value;
        console.log('this is the butt size of the request' + buttValue);
        if (typeof buttValue !== 'undefined' && buttValue !== ""){
            var buttSize = this.event.request.intent.slots.Butt_size.resolutions.resolutionsPerAuthority[0].values[0].value.name;
            buttSize = buttSize.trim();
            lovappUrl = lovappUrl + "&buttsSize=" +  buttSize;
        }
        
        var heightValue = this.event.request.intent.slots.Height.value;
        console.log('this is the height of the request' + heightValue);
        if (typeof heightValue !== 'undefined' && heightValue !== ""){
            var heightCategory = this.event.request.intent.slots.Height.resolutions.resolutionsPerAuthority[0].values[0].value.name;
            heightCategory = heightCategory.trim();
            lovappUrl = lovappUrl + "&height=" +  heightCategory;
        }
        
        var languageValue = this.event.request.intent.slots.language.value;
        if (typeof languageValue !== 'undefined' && languageValue !== ""){
             lovappUrl = lovappUrl + "&language=" +  languageValue;
        }
        
        var minAge = this.event.request.intent.slots.min_age.value;
        if (typeof minAge !== 'undefined' && minAge !== ""){
             lovappUrl = lovappUrl + "&ageFrom=" +  minAge;
        }
        
        var maxAge = this.event.request.intent.slots.max_age.value;
        if (typeof maxAge !== 'undefined' && maxAge !== ""){
             lovappUrl = lovappUrl + "&&ageTo=" +  maxAge;
        }

        this.attributes.nextPage = {
            "page" : 2,
            "pageUrl" : lovappUrl
        };
        lovappUrl = lovappUrl + '&page=1';
        console.log('this is the api path ' + lovappUrl);
  
        getLovappyAudio(lovappUrl, (theResult) => {
            
               var result = JSON.parse(theResult);
               result = result.radioPage.content;
               console.log("received : " + theResult);
               var speechOutPut = "";
               var voiceSourceList = [];
               var findTheFirstUrl = false;
               var outerContext = this;

               for (var entry = 0; entry < result.length; entry++) { 
                   
                   var audioSource = result[entry].fileUrl;
                   var audioFileName = result[entry].fileUrl.split("?")[0].split("/");
                   var wordCount = audioFileName.length;
                   audioFileName = audioFileName[wordCount - 1];
                   audioSource = alexaBaseUrl + audioFileName; 
                   voiceSourceList.push(audioSource);  
    
               }
               if (voiceSourceList.length > 0){
                   playFirstExistingAudio(voiceSourceList , (nextAudioList, playingAudioUrl) => {
                       if (nextAudioList.length > 0){
                           var speechOutPut = "<audio src='" + playingAudioUrl  + "'/>";

                           outerContext.response.shouldEndSession(false, "Reprompt your user here");
                           outerContext.attributes.audios = nextAudioList;

                           outerContext.response.speak(speechOutPut);
                           // this.response.response.shouldEndSession = false;
                           outerContext.emit(':responseReady');
                       } else {
                           outerContext.response.shouldEndSession(true, "Reprompt your user here");
                           outerContext.emit(':ask', 'Your Suggestion List is Completed');
                       }


                   });
               } else {
                   // this.response.shouldEndSession(true, "Reprompt your user here");
                   this.emit(':ask', 'Your Suggestion List is Empty Please Specify anyother criteria');
               }

            });

    
    
    // this.emit(':tell', 'Audio for ' + gender + ' skipped');
        
    },
    'AMAZON.HelpIntent': function () {
         this.emit(':ask', 'Please filter the prefered profile');
    },
    'AMAZON.CancelIntent': function () {
         this.emit(':ask', 'Goodbye you are about to leave. See you next time !!!');
    },
    'AMAZON.StopIntent': function () {
 
    },
    'AMAZON.LoopOnIntent': function () {
            var audioSource = this.attributes.audios;
            console.log("this is looping context");
            this.response.shouldEndSession(false, "Reprompt your user here");
            var firstAudioSource = audioSource[0];
            audioSource = audioSource.slice(1);
            this.attributes.audios = audioSource; 
            var speechOutPut = "<audio src='" + firstAudioSource  + "'/>";
            this.response.speak(speechOutPut);
            this.emit(':responseReady');
    },
    
    'AMAZON.NextIntent': function () {

      var voiceSourceList = this.attributes.audios;
      var outerContext = this;
      var alexaBaseUrl = "https://storage.googleapis.com/lovappy-lovdrop-alexa-testing/";
      if (voiceSourceList.length > 0){
        playFirstExistingAudio(voiceSourceList , (nextAudioList, playingAudioUrl) => {
                console.log("playing url : " + playingAudioUrl);
                var speechOutPut = "<audio src='" + playingAudioUrl  + "'/>";
                
                outerContext.response.shouldEndSession(false, "Reprompt your user here");
                outerContext.attributes.audios = nextAudioList; 
            
                outerContext.response.speak(speechOutPut);
                // this.response.response.shouldEndSession = false;
                outerContext.emit(':responseReady');

        }); 
      } else {
          var nextPage = this.attributes.nextPage;
          var lovappUrl = nextPage.pageUrl;
          var page = nextPage.page;

          this.attributes.nextPage = {
              "page" : page + 1,
              "pageUrl" : lovappUrl
          };
          lovappUrl = lovappUrl + '&page=' + page;
          getLovappyAudio(lovappUrl, (theResult) => {

              var result = JSON.parse(theResult);
              result = result.radioPage.content;
              console.log("received : " + theResult);
              var speechOutPut = "";
              var voiceSourceList = [];
              var findTheFirstUrl = false;
              var outerContext = this;

              for (var entry = 0; entry < result.length; entry++) {

                  var audioSource = result[entry].fileUrl;
                  var audioFileName = result[entry].fileUrl.split("?")[0].split("/");
                  var wordCount = audioFileName.length;
                  audioFileName = audioFileName[wordCount - 1];
                  audioSource = alexaBaseUrl + audioFileName;
                  voiceSourceList.push(audioSource);

              }
              if (voiceSourceList.length > 0){
                  playFirstExistingAudio(voiceSourceList , (nextAudioList, playingAudioUrl) => {
                      if (nextAudioList.length > 0){
                          var speechOutPut = "<audio src='" + playingAudioUrl  + "'/>";

                          outerContext.response.shouldEndSession(false, "Reprompt your user here");
                          outerContext.attributes.audios = nextAudioList;

                          outerContext.response.speak(speechOutPut);
                          // this.response.response.shouldEndSession = false;
                          outerContext.emit(':responseReady');
                      } else {
                          outerContext.response.shouldEndSession(true, "Reprompt your user here");
                          outerContext.emit(':ask', 'Your Suggestion List is Completed');
                      }


                  });
              } else {
                  // this.response.shouldEndSession(true, "Reprompt your user here");
                  this.emit(':ask', 'Your Suggestion List is Empty Please Specify anyother criteria');
              }

          });

        this.response.shouldEndSession(true, "Reprompt your user here");
        this.emit(':ask', 'Your Suggestion List is Completed');
      }
      
    },

    'Unhandled': function () {
     const speech_output = 'Goodbye and take care!'
     this.emit(':tell', speech_output);
},

};

exports.handler = function (event, context, callback) {
    const alexa = Alexa.handler(event, context, callback);
    alexa.registerHandlers(handlers);
    alexa.execute();
};

function getLovappyAudio(lovappUrl , callback) {
        var options = {
        hostname: "lovappy.com",
        path: lovappUrl,
        method: "GET",
        
    };
    var req = https.request(options, res => {
        res.setEncoding('utf8');
        var responseString = "";
        
        res.on('data', chunk => {
            responseString = responseString + chunk;
        });
        
        res.on('end', () => {
            callback(responseString);
        });

    });
    req.end();
}

function playFirstExistingAudio(audioList, callback) {
      var nextVoiceSourceList = audioList;

      if (audioList.length > 0){
        urlExists(audioList[0], function(err, exists) {
              nextVoiceSourceList = nextVoiceSourceList.slice(1);
              if (exists == "true" || exists == true || exists){
                  console.log("can play this url " + audioList[0]);
                  callback(nextVoiceSourceList, audioList[0])
              } else {
                  console.log("existence status : " + exists);
                  console.log("iterating the nexe method ");
                  playFirstExistingAudio(nextVoiceSourceList, callback)
              }
        });


      }
}

